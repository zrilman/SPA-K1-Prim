#include "Prim.h"


void main()
{
	int mat[M][M] = { {0,305,392,302,1148,36} , {305,0,429,160,1206,280} , {392,429,0,555,787,410}
				   , {302,160,555,0,1341,268} , {1148,1206,787,1341,0,1174} , {36,280,410,268,1174,0 } };
	char names[M][N] = { "Oslo","Bergen","Trondheim","Stavanger","Tromso","Drammen" };

	printMatrix(mat, names);
	Prim(mat, names);

	getchar();
}
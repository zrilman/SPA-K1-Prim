#include "Prim.h"

void printMatrix(int mat[][M], char names[][N])
{
	printf("Adjacency matrix:\n");
	for (int i = 0; i < M; ++i)
	{
		printf("%-10s: ", names[i]);
		for (int j = 0; j < M; ++j)
			printf("%4d ", mat[i][j]);
		printf("\n");
	}
	printf("\n");
}

void Prim(int mat[][M], char names[][N])
{
	int visit[M] = { 0 };
	int u, v, min;
	int num = 0, n = M, sum = 0;
	char a[N], b[N];

	printf("==============================\n");

	visit[0] = 1;

	while (num < n - 1) // MST sa n cvorova ima n-1 grana
	{
		min = 9999;
		for (int i = 0; i < n; ++i)
			for (int j = 0; j < n; ++j)
				if (mat[i][j] != 0 && mat[i][j] < min && visit[i])
				{
					min = mat[i][j];
					strcpy(a, names[i]);
					strcpy(b, names[j]);
					u = i;
					v = j;
				}

		if (visit[u] == 0 || visit[v] == 0) // Izvrsice se samo ako jedan od dva cvora nije posjecen,u slucaju da (ni)su oba posjecena nece se ispisati putanja
		{
			printf("(%-10s- %-10s,%4d)\n", a, b, min);
			visit[v] = 1;
			sum += min;
			num++;
		}
		mat[u][v] = mat[v][u] = 9999; // Sprjecava ponovni rad sa putanjom minimalne duzine
	}
	printf("------------------------------\n");
	printf("Total cost: %4d\n", sum);
	printf("==============================\n");


	
	for (int i = 0; i < n; ++i)
	{
		printf("\n");
		for (int j = 0; j < n; ++j)
			printf("%4d ", mat[i][j]);
	}
}